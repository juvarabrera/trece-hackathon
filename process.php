<?php
$backDir = '';
if(file_exists($backDir."library/Config.php"))
	require_once($backDir."library/Config.php"); 

function showSnackbar($show) {
	echo '<script>showSnackbar("'.$show.'");</script>';
}
function showSnackbarMsg($show) {
	echo '<script>showSnackbarMsg("'.$show.'");</script>';
}
function showDialogBox($show) {
	echo '<script>showDialogBox("'.$show.'");</script>';
}
function showBottomSheet($show) {
	echo '<script>showBottomSheet("'.$show.'");</script>';
}
function hideElements() {
	echo '<script>showElement("none");</script>';
}

function scriptCheckedData($id) {
?>
	<script>
	$(document).ready(function() {
		$("<?php echo $id; ?> #chkAll input").change(function() {
			if(this.checked) {
				$("<?php echo $id; ?> .checkData input").each(function() {
					if(!this.checked) {
						$checkedData.push($(this).attr("value"));
					}
				});
			} else {
				$("<?php echo $id; ?> .checkData input").each(function() {
					$index = $checkedData.indexOf($(this).attr("value"));
					$checkedData.splice($index, 1);
				});
			}
			if(this.checked) {
				$("<?php echo $id; ?> .checkData input").prop("checked", true);
			} else {
				$("<?php echo $id; ?> .checkData input").prop("checked", false);
			}
			updateDataAction();
		});
		$("<?php echo $id; ?> .checkData input").change(function() {
			$n = 1;
			$num = 0;
			$("<?php echo $id; ?> .checkData input").each(function() {
				if(!this.checked)
					$n = 0;
				else
					$num++;
			});
			if(this.checked) {
				$checkedData.push($(this).attr("value"));
			} else {
				$index = $checkedData.indexOf($(this).attr("value"));
				$checkedData.splice($index, 1);
			}
			if($n == 1)
				$("<?php echo $id; ?> #chkAll input").prop("checked", true);
			else
				$("<?php echo $id; ?> #chkAll input").prop("checked", false);
			updateDataAction();
		});
	});
	function updateDataAction() {
		if($checkedData.length != 0) {
			$("#data-action-bar .menu-title .title").html($checkedData.length + " selected");
			showDataAction(true);
		} else {
			$("#numDataSelected").html("");
			showDataAction(false);
		}
	}
	$("#data-action-bar #btnSelectAll").click(function() {
		$(".checkData input").each(function() {
			if(!this.checked) {
				$checkedData.push($(this).attr("value"));
			}
		});
		$("#chkAll input").prop("checked", true);
		$(".checkData input").prop("checked", true);
		updateDataAction();
	});
	$("#data-action-bar #btnSelectOff").click(function() {
		deselectData();
	});
	function deselectData() {
		$checkedData = [];
		$("#chkAll input").prop("checked", false);
		$(".checkData input").prop("checked", false);
		updateDataAction();
	}
	</script>
<?php
}
function showPagination($query, $filter, $rowPerPage, $p, $refreshList) {
?>
	<div class="card button-container">
		<?php
		$query = mysql_query("$query $filter");
		$totalRecords = mysql_num_rows($query);
		$totalPages = 1;
		if($rowPerPage != 0)
			$totalPages = ceil($totalRecords / $rowPerPage);
		?>
		<div class="table">
			<div class="row">
				<div class="cell compact">
					<ul class="button-container divider">
						<li><a <?php if($p != 1) echo 'id="btnFirst" '; ?>onclick class="flat_button"><span class="flat_icon ic_double-left compact"></span></a></li>
						<li><a <?php if($p != 1) echo 'id="btnPrev" '; ?>onclick class="flat_button"><span class="flat_icon ic_left compact"></span></a></li>
					</ul>
				</div>
				<div class="cell">
					<select id="ddlPage">
						<?php
						for($i = 1; $i <= $totalPages; $i++) {
							$selected = "";
							if($p == $i) {
								$selected = " selected";
							}
							echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
						}
						?>
					</select>
				</div>
				<div class="cell compact">
					<ul class="button-container right divider">
						<li><a <?php if($p != $totalPages) echo 'id="btnNext" '; ?>onclick class="flat_button"><span class="flat_icon ic_right compact"></span></a></li>
						<li><a <?php if($p != $totalPages) echo 'id="btnLast" '; ?>onclick class="flat_button"><span class="flat_icon ic_double-right compact"></span></a></li>
					</ul>
				</div>
			</div>
		</div>
		<script>
		$(document).ready(function() {
			$("#btnFirst").click(function() {
				$("#frmSearch input[name='p']").val(1);
				<?php echo $refreshList; ?>
			});
			$("#btnPrev").click(function() {
				$("#frmSearch input[name='p']").val(parseInt($("#frmSearch input[name='p']").val(), 10)-1);
				<?php echo $refreshList; ?>
			});
			$("#btnNext").click(function() {
				$("#frmSearch input[name='p']").val(parseInt($("#frmSearch input[name='p']").val(), 10)+1);
				<?php echo $refreshList; ?>
			});
			$("#btnLast").click(function() {
				$("#frmSearch input[name='p']").val(<?php echo $totalPages; ?>);
				<?php echo $refreshList; ?>
			});
			$("#ddlPage").change(function() {
				$val = $(this).val();
				$("#frmSearch input[name='p']").val($val);
				<?php echo $refreshList; ?>
			});
		});
		</script>
	</div>
<?php
}


if(isset($_GET['action'])) {
	$action = $_GET['action'];
	if($action == "show-bottom-sheet") {
		if(isset($_POST['name'])) {
			$name = $_POST['name'];
			if(isset($_POST['id'])) 
				$bs_id = $_POST['id'];
			if(file_exists("content/bottom-sheet/$name.php"))
				require_once("content/bottom-sheet/$name.php");
			else
				require_once("content/bottom-sheet/null.php");
		} else 
			require_once("content/bottom-sheet/null.php");
	} elseif($action == "show-dialog-box") {
		if(isset($_POST['name'])) {
			$name = $_POST['name'];
			if(isset($_POST['id'])) 
				$db_id = $_POST['id'];
			if(file_exists("content/dialog-box/$name.php"))
				require_once("content/dialog-box/$name.php");
			else 
				require_once("content/dialog-box/null.php");
		} else 
			require_once("content/dialog-box/null.php");
	} elseif($action == "show-snackbar") {
		if(isset($_POST['name'])) {
			$name = $_POST['name'];
			if(isset($_POST['id'])) 
				$sb_id = $_POST['id'];
			if(file_exists("content/snackbar/$name.php"))
				require_once("content/snackbar/$name.php");
			else 
				require_once("content/snackbar/null.php");
		} else 
			require_once("content/snackbar/null.php");
	} elseif($action == "getdataaction") {
		if(isset($_POST['module'])) {
			$module = $_POST['module'];
			if($module == "barangay") {
			?>
				<li><a id="btnDeleteSelected"><span class="flat_icon ic_delete_black trans_icon"></span>Delete</a></li>
				<script>
				$("#data-action-bar #btnDeleteSelected").click(function() {
					showDialogBox("deletebarangay_data", $checkedData);
				});
				</script>
			<?php
			} elseif(in_array($module, array("bhw", "rhm", "rhu", "pho"))) {
			?>
				<li><a id="btnDeleteSelected"><span class="flat_icon ic_delete_black trans_icon"></span>Delete</a></li>
				<script>
				$("#data-action-bar #btnDeleteSelected").click(function() {
					showDialogBox("delete<?php echo $module; ?>_data", $checkedData);
				});
				</script>
			<?php
			} elseif($module == "accounts") {
			?>
				<li><a id="btnDeleteSelected"><span class="flat_icon ic_delete_black trans_icon"></span>Delete</a></li>
				<script>
				$("#data-action-bar #btnDeleteSelected").click(function() {
					showDialogBox("deleteaccounts_data", $checkedData);
				});
				</script>
			<?php
			} elseif($module == "household") {
			?>
				<li><a id="btnDeleteSelected"><span class="flat_icon ic_delete_black trans_icon"></span>Delete</a></li>
				<script>
				$("#data-action-bar #btnDeleteSelected").click(function() {
					showDialogBox("deletehousehold_data", $checkedData);
				});
				</script>
			<?php
			}
		}
	} elseif($action == "login") {
		if(isset($_POST['username'], $_POST['password'])) {
			$u = $_POST['username'];
			$p = $_POST['password'];
			$check = $lib->getRow("Account", "*", "Username = '$u' AND Password = '$p'");
			if(!empty($check)) {
				 $_SESSION["loggedID"] = $lib->getSingleData("Account", "ID", "Username = '$u'");
			}
			header("Location: index.php");
		}
	} elseif($action == "logout") {
		session_destroy();
		header("Location: index.php");
	} elseif($action == "changepassword") {
		if(isset($_POST['old'], $_POST['new'], $_POST['new2'])) {
			$old = mysql_escape_string($_POST['old']);
			$new = mysql_escape_string($_POST['new']);
			$new2 = mysql_escape_string($_POST['new2']);
			$curpass = $lib->getSingleData("Account", "Password", "ID = '$loggedID'");
			if($curpass == $old && $new == $new2) {
				$q1 = mysql_query("UPDATE Account SET Password = '$new' WHERE ID = '$loggedID'");
				if($q1)
					header("Location: index.php?settings&pass_success");
				else
					header("Location: index.php?settings&pass_error");
			} else
				header("Location: index.php?settings&pass_error");
		} else
			header("Location: index.php?settings&pass_error");
	}








	elseif($action == "listbarangay") {
	?>
	<div class="card">
		<a onclick="showBottomSheet('addbarangay');" class="float_button pos_top_right ic_plus_white icon_medium"></a>
		<table class="list" id="tableListBarangay">
			<tr class="title">
				<td width="1px">
					<label id="chkAll"><input type="checkbox"><span></span></label>
				</td>
				<td>Name</td>	
				<td></td>
			</tr>
			<?php
			$filter = "";
			if(isset($_POST['city'])) {
				$city = $_POST['city'];
				$filter .= " AND City = '$city'";
			}
			$x = $lib->getData("Barangay", "*", "1=1 $filter");
			foreach($x as $data) {
			?>
			<tr>
				<td>
					<label class="checkData" id="chk_<?php echo $data["ID"]; ?>"><input type="checkbox" value="<?php echo $data["ID"]; ?>"><span></span></label>
				</td>
				<td><?php echo $data["Name"]; ?></td>
				<td>
					<ul class="button-container">
						<li><a id="btnEdit_<?php echo $data["ID"]; ?>" class="flat_icon_20 ic_pencil showhover"></a></li>
					</ul>
				</td>
			</tr>
			<script>
			$(document).ready(function() {
				$("#tableListBarangay #btnEdit_<?php echo $data["ID"]; ?>").click(function() {
					showBottomSheet('editbarangay', '<?php echo $data["ID"]; ?>');
				})
			})
			</script>
			<?php
			}
			scriptCheckedData("#tableListBarangay");
			if(empty($x)) {
			?>
			<tr>
				<td></td>
				<td colspan="2" align="center">
					No result found.
				</td>
			</tr>
			<script>
			$(document).ready(function() {
				$("#tableListBarangay #chkAll").css({"visibility": "hidden"});
			});
			</script>
			<?php
			}
			?>
		</table>
	</div>
	<script>
	$.ajax({
		type: "post",
		cache: true,
		url: "process.php?action=getdataaction",
		data: {module: "barangay"},
		success: function(html) {
			$("#data-action-bar #actions").html(html);
		}
	})
	</script>
	<?php
	} elseif($action == "listaccounts") {
		if(isset($_POST['p'], $_POST['pp'], $_POST['barangay'], $_POST['type'])) {
			$p = $_POST['p'];
			$barangay = $_POST['barangay'];
			$type = $_POST['type'];
			$rowPerPage = $_POST['pp'];
			$startFrom = ($p-1) * $rowPerPage;
			$filter = "";
			if($barangay == "none") {
				$filter .= " AND Barangay LIKE '%//0//%' ";
			} elseif($barangay == "any") {
				$filter .= " ";
			} elseif($barangay != "all") {
				$filter .= " AND Barangay LIKE '%//$barangay//%' ";
			}

			if($type != "any") {
				$filter .= " AND Type = '$type'";
			}
			$additional = "Type != 'Administrator' $filter LIMIT $startFrom, $rowPerPage";
			if($rowPerPage == 0) 
				$additional = "Type != 'Administrator' $filter";
			
	?>
	<div class="card">
		<a onclick="showBottomSheet('addaccounts');" class="float_button pos_top_right ic_plus_white icon_medium"></a>
		<table class="list" id="tableListAccounts">
			<tr class="title">
				<td width="1px">
					<label id="chkAll"><input type="checkbox"><span></span></label>
				</td>
				<td colspan="2">
					<?php echo $results = $lib->getNum("Account", "Type != 'Administrator' $filter"); echo ($result > 1) ? ' results' : ' result'; ?>
				</td>
			</tr>
			<?php
			$tableData = $lib->getData("Account", "*", $additional);
			foreach($tableData as $data) {
			?>
			<tr>
				<td>
					<label class="checkData" id="chk_<?php echo $data["ID"]; ?>"><input type="checkbox" value="<?php echo $data["ID"]; ?>"><span></span></label>
				</td>
				<td class="primary">
					<span><?php echo $data["Username"]; ?></span>
					<span><?php echo $data["ID"]; ?></span>
				</td>
				<td>
					<ul class="button-container">
						<li><a id="btnEdit_<?php echo $data["ID"]; ?>" class="flat_icon_20 ic_pencil showhover"></a></li>
					</ul>
				</td>
			</tr>
			<script>
			$(document).ready(function() {
				$("#tableListAccounts #btnEdit_<?php echo $data["ID"]; ?>").click(function() {
					showBottomSheet('editaccounts', '<?php echo $data["ID"]; ?>');
				})
			})
			</script>
			<?php
			}
			scriptCheckedData("#tableListAccounts");
			if(empty($tableData)) {
			?>
			<tr>
				<td></td>
				<td colspan="2" align="center">
					No result found.
				</td>
			</tr>
			<script>
			$(document).ready(function() {
				$("#tableListAccounts #chkAll").css({"visibility": "hidden"});
			});
			</script>
			<?php
			}
			?>
		</table>
	</div>
	<script>
	$.ajax({
		type: "post",
		cache: true,
		url: "process.php?action=getdataaction",
		data: {module: "accounts"},
		success: function(html) {
			$("#data-action-bar #actions").html(html);
		}
	})
	</script>
	<?php
			showPagination("SELECT * FROM Account WHERE Type != 'Administrator'", $filter, $rowPerPage, $p, "refreshListAccounts();");
		}
	} elseif($action == "listhousehold") {
		if(isset($_POST['p'], $_POST['pp'], $_POST['name'], $_POST['barangay'])) {
			$p = $_POST['p'];
			$name = $_POST['name'];
			$barangay = $_POST['barangay'];
			$rowPerPage = $_POST['pp'];
			$startFrom = ($p-1) * $rowPerPage;
			$filter = "";
			if($barangay == "none") {
				$filter .= " AND Barangay LIKE '%//0//%' ";
			} elseif($barangay == "any") {
				$filter .= " ";
			} elseif($barangay != "all") {
				$filter .= " AND Barangay LIKE '%//$barangay//%' ";
			}

			$filter .= " AND (CONCAT(FirstName, ' ', MiddleName, ' ', LastName) LIKE '%$name%' OR HouseholdID LIKE '%$name%') ";

			$additional = "1=1 $filter LIMIT $startFrom, $rowPerPage";
			if($rowPerPage == 0) 
				$additional = "1=1 $filter";
			
	?>
	<div class="card">
		<a href="?household-master-data&add" class="float_button pos_top_right ic_plus_white icon_medium"></a>
		<table class="list" id="tableListPatient">
			<tr class="title">
				<td width="1px">
					<label id="chkAll"><input type="checkbox"><span></span></label>
				</td>
				<td colspan="2">
					<?php echo $results = $lib->getNum("Household", "1=1 $filter"); echo ($result > 1) ? ' results' : ' result'; ?>
				</td>
			</tr>
			<?php
			$tableData = $lib->getData("Household", "*", $additional);
			foreach($tableData as $data) {
			?>
			<tr>
				<td>
					<label class="checkData" id="chk_<?php echo $data["HouseholdID"]; ?>"><input type="checkbox" value="<?php echo $data["HouseholdID"]; ?>"><span></span></label>
				</td>
				<td class="primary">
					<span><?php echo $lib->getNameFormat("l, f M.", $data["HouseholdID"]); ?></span>
					<span><?php echo $data["HouseholdID"]; ?></span>
				</td>
				<td>
					<ul class="button-container">
						<li><a href="?household-master-data&edit=<?php echo $data["HouseholdID"]; ?>" target="_blank" class="flat_icon_20 ic_pencil showhover"></a></li>
						<li><a id="btnReport_<?php echo $data["HouseholdID"]; ?>" target="_blank" class="flat_icon_20 ic_pencil showhover"></a></li>
					</ul>
				</td>
			</tr>
			<script>
			$(document).ready(function() {
				$("#tableListPatient #btnReport_<?php echo $data["HouseholdID"]; ?>").click(function() {
					showBottomSheet('reporthousehold', '<?php echo $data["HouseholdID"]; ?>');
				})
			})
			</script>
			<?php
			}
			scriptCheckedData("#tableListPatient");
			if(empty($tableData)) {
			?>
			<tr>
				<td></td>
				<td colspan="2" align="center">
					No result found.
				</td>
			</tr>
			<script>
			$(document).ready(function() {
				$("#tableListPatient #chkAll").css({"visibility": "hidden"});
			});
			</script>
			<?php
			}
			?>
		</table>
	</div>
	<script>
	$.ajax({
		type: "post",
		cache: true,
		url: "process.php?action=getdataaction",
		data: {module: "household"},
		success: function(html) {
			$("#data-action-bar #actions").html(html);
		}
	})
	</script>
	<?php
			showPagination("SELECT * FROM Account WHERE Type != 'Administrator'", $filter, $rowPerPage, $p, "refreshListPatient();");
		}
	}






















	elseif($action == "addbarangay") {
		if(isset($_POST['name'] , $_POST['city'])) {
			$name = $_POST['name'];
			$city = $_POST['city'];
			if($name != "") {
				mysql_query("INSERT INTO Barangay (Name, City) VALUES ('$name', '$city')");
				hideElements();
				showSnackbar("add_success");
			} else {
				showSnackbarMsg("Invalid name");
			}
		}
	} elseif($action == "editbarangay") {
		if(isset($_POST['name'], $_POST['city'], $_POST['id'])) {
			$id = $_POST['id'];
			$name = $_POST['name'];
			$city = $_POST['city'];
			if($name != "") {
				mysql_query("UPDATE Barangay SET Name = '$name', City = '$city' WHERE ID = '$id'");
				hideElements();
				showSnackbar("edit_success");
			} else {
				showSnackbarMsg("Invalid name");
			}
		}
	} elseif($action == "deletebarangay_data") {
		if(isset($_POST['checkedData'])) {
			$checkedData = $_POST['checkedData'];
			foreach($checkedData as $id) {
				mysql_query("DELETE FROM Barangay WHERE ID = '$id'");
			}
			showSnackbar("delete_success");
		}
	} elseif($action == "addaccounts") {
		if(isset($_POST['username'], $_POST['password'], $_POST['type'])) {
			$username = $_POST['username'];
			$password = $_POST['password'];
			$type = $_POST['type'];
			if($username != "" && $password != "") {
				$check = $lib->getData("Account", "*", "Username = '$username'");
				if(sizeof($check) == 0) {
					$id = $lib->generateAccountID();
					mysql_query("INSERT INTO Account (ID, Username, Password, Type, Barangay) VALUES ('$id', '$username', '$password', '$type', '//0//')");
					hideElements();
					showSnackbar("add_success");
				} else {
					showSnackbarMsg("Username already exists");
				}
			} else {
				showSnackbarMsg("Invalid input");
			}
		}
	} elseif($action == "editaccounts") {
		if(isset($_POST['barangay'], $_POST['id'])) {
			$id = $_POST['id'];
			$barangay = $_POST['barangay'];
			$barangay = $lib->arrayPHPtoSQL($barangay);
			mysql_query("UPDATE Account SET Barangay = '$barangay' WHERE ID = '$id'");
			hideElements();
			showSnackbar("edit_success");
		}
	} elseif($action == "deleteaccounts_data") {
		if(isset($_POST['checkedData'])) {
			$checkedData = $_POST['checkedData'];
			foreach($checkedData as $id) {
				mysql_query("DELETE FROM Account WHERE ID = '$id'");
			}
			showSnackbar("delete_success");
		}
	} elseif($action == "addhousehold") {
		if(isset($_POST['barangayid'])) {
			$columns = array("HouseholdID", "BarangayID", "PhilHealth", "NHTS", "FirstName", "MiddleName", "LastName", "NoStreet", "City", "Barangay", "Province", "MobileNo", "Gender", "CivilStatus", "BirthDate", "BloodType", "FamilyPosition", "AgeClassification", "Toilet", "WaterSource");
			
			$values = array();
			foreach($_POST as $key => $value) {
				$values[$key] = $_POST[$key];
			}
			if($values['familyposition'] == "Iba pa") 
				$values['familyposition'] = $values['familyposition_str'];
			unset($values['familyposition_str']);
			$query_value = "";
			foreach($columns as $col) {
				$query_value .= "'".$values[strtolower($col)]."',";
			}
			$query_value = rtrim($query_value, ",");
			$query = "INSERT INTO Household (".implode(",", $columns).") VALUES ($query_value)";
			mysql_query($query);
			header("Location: index.php?household-master-data"); 
		}
	} elseif($action == "edithousehold") {
		if(isset($_POST['barangayid'])) {
			$columns = array("HouseholdID", "BarangayID", "PhilHealth", "NHTS", "FirstName", "MiddleName", "LastName", "NoStreet", "City", "Barangay", "Province", "MobileNo", "Gender", "CivilStatus", "BirthDate", "BloodType", "FamilyPosition", "AgeClassification", "Toilet", "WaterSource");
			
			$values = array();
			foreach($_POST as $key => $value) {
				$values[$key] = $_POST[$key];
			}
			if($values['familyposition'] == "Iba pa") 
				$values['familyposition'] = $values['familyposition_str'];
			unset($values['familyposition_str']);
			$query_value = "";
			foreach($columns as $col) {
				if($col != "HouseholdID")
					$query_value .= $col." = '".$values[strtolower($col)]."',";
			}
			$query_value = rtrim($query_value, ",");
			$query = "UPDATE Household SET $query_value WHERE HouseholdID = '".$values['householdid']."'";
			
			mysql_query($query);
			header("Location: index.php?household-master-data"); 
		}
	} elseif($action == "deletehousehold_data") {
		if(isset($_POST['checkedData'])) {
			$checkedData = $_POST['checkedData'];
			foreach($checkedData as $id) {
				mysql_query("DELETE FROM Household WHERE HouseholdID = '$id'");
			}
			showSnackbar("delete_success");
		}
	}











	elseif($action == "getageclassification_question") {
		if(isset($_POST['ageclassification'])) {
			$ac = $_POST['ageclassification'];
			if($ac == $lib->ageClassification[0]) {
			?>
			<h4><?php echo $ac; ?></h4>
			<table class="form-container">
				<tr>
					<td>
						<label>Klase ng Home Based Record na mayroon ang newborn:</label>
						<table width="100%" cellspacing="0px" cellpadding="0px">
							<?php
							$options = array("MCB", "ECCD");
							foreach($options as $op) {
								echo '<tr><td colspan="2"><label><input type="radio" name="homebasedrecord" value="'.$op.'"><span></span>'.$op.'</label></td></tr>';
							}
							?>
							<tr><td><label><input type="radio" name="homebasedrecord"><span></span>Iba pa</label></td><td><input type="text" name="homebasedrecord_str"></td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<label>Timbang ng bagong silang (kg):</label>
						<input type="number" name="weight" placeholder="Weight (kg)">
					</td>
				</tr>
				<tr>
					<td>
						<label>Haba ng bagong silang (cm):</label>
						<input type="number" name="height" placeholder="Height (cm)">
					</td>
				</tr>
				<tr>
					<td>
						<label>Lugar kung saan ipinanganak</label>
						<table width="100%" cellspacing="0px" cellpadding="0px">
						<?php
						$options = array("Bahay", "Rural Health Unit/ Health Center", "Private (Hospital, lying-in within the municipality/city)", "Outside Municipality/City", "Others");
						for($i = 0; $i < sizeof($options); $i++) {
							if($i > 1)
								echo '<tr><td><label><input type="radio" name="location" value="'.$options[$i].'"><span></span>'.$options[$i].'</label></td><td><input type="text" name="location_'.$i.'"></td></tr>';
							else 
								echo '<tr><td colspan="2"><label><input type="radio" name="location" value="'.$options[$i].'"><span></span>'.$options[$i].'</label></td></tr>';
						}
						?>
						</table>
					</td>
				</tr>
			</table>
			<h4>Part 1: Tanong at Mensaheng Pangkalusugan</h4>
			<table class="form-container">
				<tr>
					<td>
						<hr>
						<span class="title">A1.</span><label>Nakikita mo ba sa iyong bagong silang ang alinman sa mga sumusunod na palatandaan ng panganib?</label>
						<table width="100%" cellspacing="0px" cellpadding="0px">
							<?php
							$options = array("Kombulsiyon/pagtirik ng mga mata", "Tumigil sa pagsuso/mahinang pagsuso", "Mainit o malamig na temperatura", "Mabahong tagas o dugo sa pusod", "Madilaw na talampakan/mata/balat", "Wala o bahagyang pagkilos ng sanggol", "Mabilis o hirap na paghinga");
							$num = 0;
							foreach($options as $op) {
							?>
							<tr><td><label><input type="checkbox" name="a1[]" id="a1_<?php echo $num; ?>" value="<?php echo $op; ?>"><span></span><?php echo $op; ?></label></td></tr>
							<script>
							$(document).ready(function() {
								$("#a1_<?php echo $num; ?>").click(function() {
									if($(this).is(":checked"))
									showBottomSheet('message_a1');
								})
							})
							</script>
							<?php
								$num++;
							}
							?>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<hr>
						<span class="title">A2.</span><label>Nabigyan ba ang bata ng alinman sa sumusunod?</label>
						<table width="100%" cellspacing="0px" cellpadding="0px">
							<?php
							$options = array("Newborn Screening (NBS)", "Bakuna na BCG", "Bakuna na Hepatitis B");
							$num = 0;
							foreach($options as $op) {
							?>
							<tr><td><label><input type="checkbox" name="a2[]" id="a2_<?php echo $num; ?>" value="<?php echo $op; ?>"><span></span><?php echo $op; ?></label></td><td><input type="date" name="a2_<?php echo $num; ?>"></td></tr>
							<script>
							$(document).ready(function() {
								$("#a2_<?php echo $num; ?>").click(function() {
									if($(this).is(":checked"))
									<?php
									if($num == 0) 
										echo "showBottomSheet('message_a2_1');";
									else
										echo "showBottomSheet('message_a2_2');";
									?>
								})
							})
							</script>
							<?php
								$num++;
							}
							?>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<label>Pinanggalingan ng serbisyong natangap:</label>
						<table width="100%" cellspacing="0px" cellpadding="0px">
							<?php
							$options = array("Health Center/RHU", "Brgy. Health Station", "Pribadong Clinic");
							$num = 0;
							foreach($options as $op) {
							?>
							<tr><td><label><input type="checkbox" name="servicesource[]" value="<?php echo $op; ?>"><span></span><?php echo $op; ?></label></td></tr>
							<?php
								$num++;
							}
							?>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<hr>
						<span class="title">A3.</span><label>Ang ipinapasuso mo ba sa iyong sanggol ay purong "breastmilk" o gatas ng ina lamang?</label>
						<table width="100%" cellspacing="0px" cellpadding="0px">
							<?php
							$options = array("Oo", "Hindi");
							$num = 0;
							foreach($options as $op) {
							?>
							<tr><td><label><input type="radio" name="a3" id="a3_<?php echo $num; ?>" value="<?php echo $op; ?>"><span></span><?php echo $op; ?></label></td></tr>
							<script>
							$(document).ready(function() {
								$("#a3_<?php echo $num; ?>").click(function() {
									if($(this).is(":checked"))
									<?php
									if($num == 0)
										echo "showDialogBox('batiin');";
									else
										echo "showBottomSheet('message_a3');";
									?>
								})
							})
							</script>
							<?php
								$num++;
							}
							?>
						</table>
					</td>
				</tr>
			</table>
			<?php
			} elseif($ac == $lib->ageClassification[1]) {
			?>
			
			<?php
			}
		}
	} else {
		header("Location: index.php");
	}
} else {
	header("Location: index.php");
}
?>