<?php
require_once("library/Hackathon.php");
require_once("library/Database.php");
date_default_timezone_set('Asia/Manila');

ini_set('display_errors', 0);
ini_set('max_execution_time', 600);
error_reporting(0);


if($_SERVER["HTTP_HOST"] == "localhost") {
	$mysql_host = "localhost";
	$mysql_database = "hackathon";
	$mysql_user = "root";
	$mysql_password = "";
} else {
	$mysql_host = "mysq.000webhost.com";
	$mysql_database = "a7193";
	$mysql_user = "a7193";
	$mysql_password = "hackathon2015";
}



$oes_db = new Database($mysql_user, $mysql_password, $mysql_host, $mysql_database);
$oes_db->ConnectDB();
$lib = new Hackathon();

session_start();
$loggedIn = false;

if(isset($_SESSION['loggedID'])) {
	$loggedIn = true;
	$loggedID = $_SESSION['loggedID'];
	$check = $lib->getRow("Account", "*", "ID = '$loggedID'");
	if(isset($_GET['logout']) || sizeof($check) == 0) {
		header("Location: process.php?action=logout");
	}
}

?>