<?php
class Hackathon {
	protected $loggedID;

	public $ageClassification = array(
		"Bagong panganak (0-28 na araw)", //Diagnosis_BP 
		"Sanggol (11 na buwan na gulang pababa)", //Diagnosis_S 
		"Under five (1-5 na taong gulang)",  //Diagnosis_UF
		"Iba pang edad (5-9 na taong gulang)",
		"Adolescent (10-19 na taong gulang)",
		"Babae na kasalukuyang buntis (15-49 na taong gulang)", //Diagnosis_BKB
		"Babae na kakapanganak pa lang at hindi pa nakakalipas ng 42 na araw nung nanganak (15-49 na taong gulang)", //Diagnosis_BK42
		"Women of Reproductive Age (15-49 na taong gulang)", //Diagnosis_WRA
		"Bata na umuubo ng dalawang linggo o higit pa (14 na taong gulang pababa)", //Diagnosis_U14B
		"Bata na umuubo ng dalawang linggo o higit pa (15 na taong gulang pataas)", //Diagnosis_U15U
		"Senior Citizen (60 na taong gulang pataas)" //Diagnosis_SC
	);

	public $cities = array("Bacoor", "Cavite City", "Dasmariñas", "Imus", "Tagaytay", "Trece Martires", "Alfonso", "Amadeo", "Carmona", "General Emilio Aguinaldo", "General Mariano Alvarez", "General Trias", "Indang", "Kawit", "Magallanes", "Maragondon", "Mendez", "Naic", "Noveleta", "Rosario", "Silang", "Tanza", "Ternate");
	public $types = array("BHW", "RHM", "RHU", "PHO");

	function loggedUser($loggedID) {
		$this->loggedID = $loggedID;
	}

	function getAge($birthDate) {
		return date_diff(date_create($birthDate), date_create('now'))->y;
	}

	function getNameFormat($format, $id) {
		$x = $format;
		$formats = "FfMmLl";
		for($i = 0; $i < strlen($formats); $i++) {
			$temp = substr($formats, $i, 1);
			$x = str_replace($temp, '{'.$temp.'}', $x);
		}
		$query = mysql_query("SELECT FirstName, MiddleName, LastName FROM Household WHERE HouseholdID = '$id'");
		while($row = mysql_fetch_array($query)) {
			$x = str_replace("{f}", $row["FirstName"], $x);
			$x = str_replace("{m}", $row["MiddleName"], $x);
			$x = str_replace("{l}", $row["LastName"], $x);
			$x = str_replace("{F}", substr($row["FirstName"], 0, 1), $x);
			$x = str_replace("{M}", substr($row["MiddleName"], 0, 1), $x);
			$x = str_replace("{L}", substr($row["LastName"], 0, 1), $x);
		}
		return $x;
	}

	function generateAccountID() {
		$id = "";
		$sy = date("Y");
		$query = mysql_query("SELECT * FROM Account WHERE ID LIKE '".$sy."%' ORDER BY ID DESC LIMIT 1");
		$n = 0;
		while($row = mysql_fetch_array($query)) {
			$n = 1;
			$id = $row["ID"]+1;
			break;
		}
		if($n == 0)
			$id = $sy."00001";
		return $id;
	}

	function arrayPHPtoSQL($arr) {
		$sql = "//";
		foreach($arr as $a) {
			$sql .= $a."//";
		}
		return $sql;
	}

	function arraySQLtoPHP($str) {
		$arr = array();
		return explode("//", trim($str, "//"));

	}



	function getSingleData($table, $column, $where) {
		$x = "";
		$query = mysql_query("SELECT $column FROM $table WHERE $where");
		while($row = mysql_fetch_array($query)) {
			$x = $row[$column];
			break;
		}
		return $x;
	}

	function getRow($table, $columns, $additional) {
		$x = array();
		$queryString = "";
		if($columns == "*") {
			$queryString .= "SELECT * ";
			$columns = array();
			$temp_table = explode(" INNER JOIN ", $table);
			foreach($temp_table as $t) {
				$query = mysql_query("SHOW COLUMNS FROM $t");
				while($row = mysql_fetch_array($query)) {
					$columns[] = $row["Field"];
				}
			}
		} elseif(strpos(",", $columns)) {
			$queryString .= "SELECT $columns ";
			$columns = explode(",", str_replace(" ", "", $columns));
		}
		$queryString .= "FROM $table";
		if($additional != "")
			$queryString .= " WHERE $additional";
		$query = mysql_query($queryString);
		while($row = mysql_fetch_array($query)) {
			foreach($columns as $column)
				$x[$column] = $row[$column];
			break;
		}
		return $x;
	}

	function getData($table, $columns, $additional) {
		$x = array();
		$queryString = "";
		$tables = explode(" INNER JOIN ", $table);
		if($columns == "*") {
			$queryString .= "SELECT * ";
			$columns = array();
			foreach($tables as $t) {
				$query = mysql_query("SHOW COLUMNS FROM $t");
				while($row = mysql_fetch_array($query)) {
					$columns[] = $row["Field"];
				}
			}
		} elseif(strpos(",", $columns)) {
			$queryString .= "SELECT $columns ";
			$columns = explode(",", str_replace(" ", "", $columns));
		}
		$queryString .= "FROM $table";
		if($additional != "")
			$queryString .= " WHERE $additional";
		$query = mysql_query($queryString);
		$i = 0;
		while($row = mysql_fetch_array($query)) {
			foreach($columns as $column)
				$x[$i][$column] = $row[$column];
			$i++;
		}
		return $x;
	}

	function getNum($table, $additional) {
		$x = $this->getData($table, "*", $additional);
		return sizeof($x);
	}

	function convertPHPArrayToJS($arr) {
		$x = "[";
		for($i = 0; $i < sizeof($arr); $i++) {
			if($i == 0) 
				$x .= $arr[$i];
			else
				$x .= ", ".$arr[$i];
		}
		$x .= "]";
		return $x;
	}


}
?>