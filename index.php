<?php 
	$backDir = '';
	if(file_exists($backDir."library/Config.php")) {
		require_once($backDir."library/Config.php"); 
	}
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	<title>Barangay Health Worker Household Health Profiling</title>
	<script src="<?php echo $backDir; ?>scripts/jquery.min.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo $backDir; ?>styles/skin/light/">
	<link rel="stylesheet" href="<?php echo $backDir; ?>styles/style.php">
	<script src="<?php echo $backDir; ?>scripts/smooth_scroll.js"></script>
</head>
<body name="tp">
<script src="<?php echo $backDir; ?>scripts/oslo-ui.js"></script>
<div id="app-container">
	<?php
	if($loggedIn) {
		$displayPage = "Dashboard";
		$type = $lib->getSingleData("Account", "Type", "ID = '$loggedID'");
		if($type == "Administrator") {
			$mainMenu = array(
				"Dashboard",
				"Barangay Master List",
				"Accounts",
				"Settings"
			);
		} elseif($type == "BHW") {
			$mainMenu = array(
				"Dashboard",
				"Household Master Data",
				"Alerts Overview",
				"Settings"
			);
		} elseif($type == "RHM") {
			$mainMenu = array(
				"Dashboard",
				"Household Master Data",
				"Settings"
			);
		} elseif($type == "RHU") {
			$mainMenu = array(
				"Dashboard",
				"Household Master Data",
				"Settings"
			);
		} elseif($type == "PHO") {
			$mainMenu = array(
				"Dashboard",
				"Household Master Data",
				"Settings"
			);
		}
		foreach ($mainMenu as $menu) {
			if(is_array($menu)) {
				foreach($menu[1] as $submenu) {
					if(isset($_GET[str_replace(" ", "-", strtolower($submenu))])) {
						$displayPage = $submenu;
						break;
					}
				}
			} else {
				if(isset($_GET[str_replace(" ", "-", strtolower($menu))])) {
					$displayPage = $menu;
					break;
				}
			}
		}
		$pageCode = str_replace(" ", "-", strtolower($displayPage));
		require_once("template/".$type."/action-bar.php");
		require_once("template/".$type."/float-left-menu.php");
		if(file_exists("template/".$type."/".$pageCode.".php"))
			require_once("template/".$type."/".$pageCode.".php");
		else
			require_once("template/notfound.php");
	} else {
		if(isset($_GET['login']))
			require_once("login.php");
		elseif(isset($_GET['register']))
			require_once("register.php");
		else
			require_once("login.php");
	}
	?>
	<div id="blackTrans"></div>
	<div id="bottom-sheet"><div class="loading"></div></div>
	<div id="dialog-box">
		<div class="wrapper">
			<center><div class="loading"></div></center>
		</div>
	</div>
	<div id="snackbar">
		<div class="wrapper">
		</div>
	</div>
	<div id="loading"></div>
	<div id="data-action-bar">
		<div class="row">
			<div class="menu-title">
				<span class="title"></span>
			</div>
			<div class="actions">
				<ul>
					<li><a id="btnSelectAll" class="icons action_icon ic_select-all_black gray_icon icon_medium"></a></li>
					<li><a id="btnSelectOff" class="icons action_icon ic_select-off_black gray_icon icon_medium"></a></li>
					<li><a class="icons action_icon ic_dots-vertical_black gray_icon icon_medium"></a>
					<ul class="dropdownlist" id="actions">

					</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
</body>
</html>