<span class="title">Message</span>
<p>
	Are you sure you want to delete the selected accounts?
</p>
<ul class="button-container right">
	<li><a onclick="showElement('none')" class="flat_button">No</a></li>
	<li><a id="btnSubmit" class="flat_button">Yes</a></li>
</ul>
<script>
$(document).ready(function() {
	$("#btnSubmit").click(function() {
		$("#dialog-box ul.button-container").hide();
		$("#loading").show("slow");
		$("#dialog-box").css({
			"margin-top": "-"+(($("#dialog-box").height()/2).toFixed())+"px"
		});
		$.ajax({
			type: "post",
			cache: false,
			url: "process.php?action=deleteaccounts_data",
			data: {checkedData: <?php echo $lib->convertPHPArrayToJS($db_id); ?>},
			success: function(html) {
				showElement('none');
				refreshListAccounts();
				$("#snackbar .wrapper").html(html);
			}
		});
	});
});
</script>