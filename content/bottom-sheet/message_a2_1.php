<div class="content">
	<h3>Mensahe</h3>
	<p>Mensahe patungkol sa newborn screening para sa mga bagong silang</p>
	<ul>
		<li>Mahalaga ang newborn screening (NBS) dahil makatutulong ito sa maagang pagtuklas sa mga sakit tulad ng mental retardation. Ipasuri ng NBS ang bagong silang pagkalipas ng unang 24 hanggang 48 na oras. Basahin ang bahagi ng Family Health Guide patungkol sa Pangangalaga ng Bagong Silang (pahina 6) para sa karagdagang impormasyon</li>
		<li>Dalhin ang iyong anak sa doktor, nars, midwife o alinmang skilled health provider para sa newborn screening</li>
		<li>Walang bayad ang NBSsa mga "PhilHealth accredited" na ospital  o klinika ng gobyerno para sa mga dependents ng miyembro ng PhilHealth. (Tignan ang Seksiyon A ng Inpatient coverage, p. 7 at Table 2, p. 8 ng Gabay ng Pamilya sa PhilHealth)</li>
		<li>Tingnan ang iyong <b><i>Booklet ni Nanay at ni Baby - "Ang Aking Mga Pangangailangan sa Unang Linggo ng Aking Pagsilang"</i></b>, p. 21</li>
	</ul>
	<ul class="button-container right">
		<li><a onclick="showElement('none');" target="_blank" class="raised_button">Okay</a></li>
	</ul>
</div>
<script>
$(document).ready(function() {

})
</script>