<?php
$data = $lib->getRow("Barangay", "*", "ID = '$bs_id'"); 
?>
<div class="content">
	<h3>Edit Barangay</h3>
	<table class="form-container">
		<tr>
			<td>
				<label>Barangay Name</label>
				<input type="text" name="name" placeholder="Barangay Name" value="<?php echo $data["Name"]; ?>">
			</td>
		</tr>
		<tr>
			<td>
				<label>City/Municipality</label>
				<select name="city">
					<?php
					$cities = $lib->cities;
					foreach($cities as $city) {
						$selected = "";
						if($city == $data["City"])
							$selected = " selected";
						echo '<option value="'.$city.'"'.$selected.'>'.$city.'</option>';
					}
					?>
				</select>
			</td>
		</tr>
	</table>
	<ul class="button-container right">
		<li><a onclick="showElement('none');" target="_blank" class="raised_button">Cancel</a></li>
		<li><a id="btnSubmit" target="_blank" class="raised_button">Edit</a></li>
	</ul>
</div>
<script>
$(document).ready(function() {
	$("#btnSubmit").click(function() {
		$("#bottom-sheet ul.button-container").hide();
		$("#loading").show("slow");
		$name = $("#bottom-sheet input[name='name']").val();
		$city = $("#bottom-sheet select[name='city']").val();
		$.ajax({
			type: "post",
			cache: true,
			url: "process.php?action=editbarangay",
			data: {id: '<?php echo $bs_id; ?>', name: $name, city: $city},
			success: function(html) {
				$("#bottom-sheet ul.button-container").show();
				$("#loading").hide("slow");
				$("#snackbar .wrapper").html(html);
				refreshListBarangay();
			}
		});
	});
})
</script>