<div class="content">
	<h3>Add Barangay</h3>
	<table class="form-container">
		<tr>
			<td>
				<label>Barangay Name</label>
				<input type="text" name="name" placeholder="Barangay Name">
			</td>
		</tr>
		<tr>
			<td>
				<label>City/Municipality</label>
				<select name="city">
					<?php
					$cities = $lib->cities;
					foreach($cities as $city) {
						echo '<option value="'.$city.'">'.$city.'</option>';
					}
					?>
				</select>
			</td>
		</tr>
	</table>
	<ul class="button-container right">
		<li><a onclick="showElement('none');" target="_blank" class="raised_button">Cancel</a></li>
		<li><a id="btnSubmit" target="_blank" class="raised_button">Add</a></li>
	</ul>
</div>
<script>
$(document).ready(function() {
	$("#btnSubmit").click(function() {
		$("#bottom-sheet ul.button-container").hide();
		$("#loading").show("slow");
		$name = $("#bottom-sheet input[name='name']").val();
		$city = $("#bottom-sheet select[name='city']").val();
		$.ajax({
			type: "post",
			cache: true,
			url: "process.php?action=addbarangay",
			data: {name: $name, city: $city},
			success: function(html) {
				$("#bottom-sheet ul.button-container").show();
				$("#loading").hide("slow");
				$("#snackbar .wrapper").html(html);
				refreshListBarangay();
			}
		});
	});
})
</script>