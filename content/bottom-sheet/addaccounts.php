<div class="content">
	<h3>Add Accounts</h3>
	<table class="form-container">
		<tr>
			<td>
				<label>Username</label>
				<input type="text" name="username" placeholder="Username">
			</td>
		</tr>
		<tr>
			<td>
				<label>Password</label>
				<input type="password" name="password" placeholder="Password">
			</td>
		</tr>
		<tr>
			<td>
				<label>Type</label>
				<select name="type">
					<?php
					$options = $lib->types;
					foreach($options as $op) {
						echo '<option value="'.$op.'">'.$op.'</option>';
					}
					?>
				</select>
			</td>
		</tr>
	</table>
	<ul class="button-container right">
		<li><a onclick="showElement('none');" target="_blank" class="raised_button">Cancel</a></li>
		<li><a id="btnSubmit" target="_blank" class="raised_button">Add</a></li>
	</ul>
</div>
<script>
$(document).ready(function() {
	$("#btnSubmit").click(function() {
		$("#bottom-sheet ul.button-container").hide();
		$("#loading").show("slow");
		$username = $("#bottom-sheet input[name='username']").val();
		$password = $("#bottom-sheet input[name='password']").val();
		$type = $("#bottom-sheet select[name='type']").val();
		$.ajax({
			type: "post",
			cache: true,
			url: "process.php?action=addaccounts",
			data: {username: $username, password: $password, type: $type},
			success: function(html) {
				$("#bottom-sheet ul.button-container").show();
				$("#loading").hide("slow");
				$("#snackbar .wrapper").html(html);
				refreshListAccounts();
			}
		});
	});
})
</script>