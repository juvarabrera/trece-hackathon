<div class="content">
	<h3>Mensahe</h3>
	<p>Mensahe para sa Pagpapasuso ng purong gatas ng ina lamang para sa bagong silang</p>
	<ul>
		<li>Ang pagpapasuso ng gatas ng ina sa sanggol sa unang 6 na buwan (na walang ibang kahalo tulad ng milk formula o ipinapainom na tubig at iba pang ipinapakain) ay magbibigay sa kaniya ng proteksyon laban sa iba't ibang impeksiyon tulad ng sa tainga, pagtatae at pulmonya.</li>
		<li>Sapat ang gatas ng ina sa pangangailangan ng iyong anak sa unang 6 na buwan (Ekslusibong Pagpapasuso)</li>
		<li>Pasusuhin ang bata mula pagkasilang hanggang 2 taon pataas.</li>
		<li>Tingnan ang <i><b>Booklet ni Nanay at ni Baby - "Tagubilin sa Pagpapakain"</b></i>, p. 24, para sa karagdagang impormasyon.</li>
	</ul>
	<ul class="button-container right">
		<li><a onclick="showElement('none');" target="_blank" class="raised_button">Okay</a></li>
	</ul>
</div>
<script>
$(document).ready(function() {

})
</script>