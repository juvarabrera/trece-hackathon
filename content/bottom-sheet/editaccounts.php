<?php
$data = $lib->getRow("Account", "*", "ID = '$bs_id'"); 
?>
<script>
$(document).ready(function() {
	$selectedData = [];
	$("#bottom-sheet #chkAll input").change(function() {
		if(this.checked) {
			$("#bottom-sheet .checkData input").each(function() {
				if(!this.checked) {
					$selectedData.push($(this).attr("value"));
				}
			});
		} else {
			$("#bottom-sheet .checkData input").each(function() {
				$index = $selectedData.indexOf($(this).attr("value"));
				$selectedData.splice($index, 1);
			});
		}
		if(this.checked) {
			$("#bottom-sheet .checkData input").prop("checked", true);
		} else {
			$("#bottom-sheet .checkData input").prop("checked", false);
		}
	});
	$("#bottom-sheet .checkData input").change(function() {
		$n = 1;
		$num = 0;
		$("#bottom-sheet .checkData input").each(function() {
			if(!this.checked)
				$n = 0;
			else
				$num++;
		});
		if(this.checked) {
			$selectedData.push($(this).attr("value"));
		} else {
			$index = $selectedData.indexOf($(this).attr("value"));
			$selectedData.splice($index, 1);
		}
		if($n == 1)
			$("#bottom-sheet #chkAll input").prop("checked", true);
		else
			$("#bottom-sheet #chkAll input").prop("checked", false);
	});
})
</script>
<div class="content">
	<h3>Edit Account</h3>
	<table class="list">
		<tr class="title">
			<td width="1px">
				<label id="chkAll"><input type="checkbox"><span></span></label>
			</td>
			<td>Barangay</td>
		</tr>
		<?php
		$bgryData = $lib->getData("Barangay", "*", "1=1 ORDER BY City, Name");
		$userBarangay = $lib->arraySQLtoPHP($data["Barangay"]);
		foreach($bgryData as $bgry) {
			$checked = "";
			if(in_array($bgry["ID"], $userBarangay))
				$checked = " checked";
		?>
		<tr>
			<td>
				<label class="checkData" id="chk_<?php echo $bgry["ID"]; ?>"><input type="checkbox" value="<?php echo $bgry["ID"]; ?>"<?php echo $checked; ?>><span></span></label>
			</td>
			<td><?php echo $bgry["Name"].', '.$bgry["City"]; ?></td>
		</tr>
		<?php
			if(in_array($bgry["ID"], $userBarangay)) {
		?>
			<script>
			$(document).ready(function() {
				$selectedData.push($("#bottom-sheet #chk_<?php echo $bgry["ID"]; ?> input").attr("value"));
				$n = 1;
				$num = 0;
				$("#bottom-sheet .checkData input").each(function() {
					if(!this.checked)
						$n = 0;
					else
						$num++;
					if($n == 1)
						$("#bottom-sheet #chkAll input").prop("checked", true);
					else
						$("#bottom-sheet #chkAll input").prop("checked", false);
				});
			});
			</script>
		<?php
			}
		}
		?>
	</table>
	<ul class="button-container right">
		<li><a onclick="showElement('none');" target="_blank" class="raised_button">Cancel</a></li>
		<li><a id="btnSubmit" target="_blank" class="raised_button">Edit</a></li>
	</ul>
</div>
<script>
$(document).ready(function() {
	$("#btnSubmit").click(function() {
		$("#bottom-sheet ul.button-container").hide();
		$("#loading").show("slow");
		if($selectedData.length == 0)
			$selectedData.push("0");
		$.ajax({
			type: "post",
			cache: true,
			url: "process.php?action=editaccounts",
			data: {id: '<?php echo $bs_id; ?>', barangay: $selectedData},
			success: function(html) {
				$("#bottom-sheet ul.button-container").show();
				$("#loading").hide("slow");
				$("#snackbar .wrapper").html(html);
				refreshListAccounts();
			}
		});
	});
})
</script>