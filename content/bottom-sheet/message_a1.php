<div class="content">
	<h3>Mensahe</h3>
	<p>Mensahe para sa mga palantandaan ng panganib sa kalusugan</p>
	<ul>
		<li>Dalhin ang iyong anak sa health provider kapag nakita ang alinman sa mga palatandaan ng panganib</li>
		<li>Kapag dinala ang bata patungo sa health facility:
			<ul>
				<li>Huwag hayaang malamigan ang sanggol</li>
				<li>Pasusuhin ang sanggol kada oras (Kung kaya niyang sumuso)</li>
			</ul>
		</li>
	</ul>
	<ul class="button-container right">
		<li><a onclick="showElement('none');" target="_blank" class="raised_button">Okay</a></li>
	</ul>
</div>
<script>
$(document).ready(function() {

})
</script>