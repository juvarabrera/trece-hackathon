<div id="body-container">
	<div class="content">
		<div class="bg-cover"></div>
		<div class="title">
			<h1>Barangays</h1>
		</div>
		<div class="wrapper">
			<div class="col-3">
				<div class="card" id="frmSearch">
					<h4>Search</h4>
					<table class="form-container">
						<tr>
							<td><label>City</label>
							<select name="city">
								<?php
								$options = mysql_query("SELECT DISTINCT City FROM Barangay ORDER BY City");
								while($op = mysql_fetch_array($options)) {
									echo '<option value="'.$op["City"].'">'.$op["City"].'</option>';
								}
								?>
							</select><input type="hidden" name="p" value="1"></td>
						</tr>
						<tr>
							<td><label>Results per page</label>
							<select name="pp">
							<?php
							$options = array(25,100,250,"All");
							foreach($options as $option) {
								echo '<option value="'.$option.'">'.$option.'</option>';
							}
							?>
							</select></td>
						</tr>
					</table>
					<ul class="button-container block">
						<li><a id="btnSearch" class="raised_button">Search</a></li>
					</ul>
				</div>
			</div>
			<div class="col-7" id="lstBarangay">
			
			</div>
			<script>
			function refreshListBarangay() {
				$checkedData = [];
				$("#numDataSelected").html("");
				showDataAction(false);

				$("#lstBarangay").html('<div class="card"><center><br><br><img src="images/skin/oslo/bg/loading.gif"><br><br></center></div>');
				$city = $("#frmSearch select[name=city]").val()
				$.ajax({
					type: "post",
					cache: true,
					url: "process.php?action=listbarangay",
					data: {city: $city},
					success: function(html) {
						$("#lstBarangay").html(html);
					}
				})
			}
			$(document).ready(function() {
				$("#frmSearch #btnSearch").click(function() {
					refreshListBarangay();
				})
			})
			$checkedData = [];
			refreshListBarangay();
			</script>
		</div>
	</div>
</div>