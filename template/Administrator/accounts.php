<div id="body-container">
	<div class="content">
		<div class="bg-cover"></div>
		<div class="title">
			<h1>Accounts</h1>
		</div>
		<div class="wrapper">
			<div class="col-3">
				<div class="card" id="frmSearch">
					<h4>Search</h4>
					<table class="form-container">
						<tr>
							<td><label>Barangay</label>
							<select name="barangay">
								<option value="any">Any barangay</option>
								<option value="none">No barangay</option>
								<?php
								$options = $lib->getData("Barangay", "*", "1=1 ORDER BY City, Name");
								foreach($options as $op) {
									echo '<option value="'.$op["ID"].'">'.$op["Name"].', '.$op["City"].'</option>';
								}
								?>
							</select><input type="hidden" name="p" value="1"></td>
						</tr>
						<tr>
							<td><label>Type</label>
							<select name="type">
								<option value="any">All types</option>
								<?php
								$options = $lib->types;
								foreach($options as $op) {
									echo '<option value="'.$op.'">'.$op.'</option>';
								}
								?>
							</select><input type="hidden" name="p" value="1"></td>
						</tr>
						<tr>
							<td><label>Results per page</label>
							<select name="pp">
							<?php
							$options = array(25,100,250,"All");
							foreach($options as $option) {
								echo '<option value="'.$option.'">'.$option.'</option>';
							}
							?>
							</select></td>
						</tr>
					</table>
					<ul class="button-container block">
						<li><a id="btnSearch" class="raised_button">Search</a></li>
					</ul>
				</div>
			</div>
			<div class="col-7" id="lstAccounts">
			</div>
			<script>
			function refreshListAccounts() {
				$checkedData = [];
				$("#numDataSelected").html("");
				showDataAction(false);

				$("#lstAccounts").html('<div class="card"><center><br><br><img src="images/skin/oslo/bg/loading.gif" /><br><br></center></div>');
				$barangay = $("#frmSearch select[name=barangay]").val();
				$type = $("#frmSearch select[name=type]").val();
				$p = $("#frmSearch input[name=p]").val();
				$pp = $("#frmSearch select[name=pp]").val();
				$.ajax({
					type: "post",
					cache: true,
					url: "process.php?action=listaccounts",
					data: {p: $p, pp: $pp, barangay: $barangay, type: $type},
					success: function(html) {
						$("#lstAccounts").html(html);
					}
				})
			}
			$(document).ready(function() {
				$("#frmSearch #btnSearch").click(function() {
					refreshListAccounts();
				})
			})
			refreshListAccounts();
			</script>
		</div>
	</div>
</div>