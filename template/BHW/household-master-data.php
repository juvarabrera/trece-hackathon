<?php
if(isset($_GET['add'])) {
?>
<div id="body-container">
	<div class="content">
		<div class="bg-cover"></div>
		<div class="title">
			<h1>Add Household Record</h1>
		</div>
		<div class="wrapper">
			<div class="col-6 offset-2" id="frmApplication">
				<form action="process.php?action=addhousehold" method="post">
				<div class="card">
					<h4>Identification</h4>
					<table class="form-container">
						<tr>
							<td>
								<label>Barangay</label>
								<select name="barangayid">
								<?php
								$options = $lib->getData("Barangay", "*", "1=1 ORDER BY City, Name");
								foreach($options as $op) {
									echo '<option value="'.$op["ID"].'">Barangay '.$op["Name"].', '.$op["City"].'</option>';
								}
								?>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<label>PhilHealth No.</label>
								<input type="text" name="philhealth" placeholder="PhilHealth No.">
							</td>
						</tr>
						<tr>
							<td>
								<label>NHTS</label>
								<br>
								<table width="100%">
									<tr>
										<td><label><input type="radio" name="nhts" value="Oo"><span></span>Oo</label></td>
										<td><label><input type="radio" name="nhts" value="Hindi" checked><span></span>Hindi</label></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<label>Household ID</label>
								<input type="text" name="householdid" placeholder="Household ID">
							</td>
						</tr>
					</table>
				</div>
				<div class="card">
					<h4>Personal Information</h4>
					<table class="form-container">
						<tr>
							<td>
								<label>Buong Pangalan</label>
								<table width="100%" cellspacing="0px" cellpadding="0px">
									<tr>
										<td><input type="text" name="firstname" placeholder="First Name"></td>
										<td><input type="text" name="middlename" placeholder="Middle Name"></td>
										<td><input type="text" name="lastname" placeholder="Last Name"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<label>Tirahan</label>
								<table width="100%" cellspacing="0px" cellpadding="0px">
									<tr>
										<td><input type="text" name="nostreet" placeholder="No. Street"></td>
										<td><input type="text" name="barangay" placeholder="Barangay"></td>
									</tr>
									<tr>
										<td>
											<select name="city">
												<?php
												$cities = $lib->cities;
												foreach($cities as $city) {
													echo '<option value="'.$city.'">'.$city.'</option>';
												}
												?>
											</select>
										</td>
										<td><input type="text" name="province" placeholder="Province" value="Cavite"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<label>Numero ng Cellphone/Telepono</label>
								<input type="text" name="mobileno" placeholder="Numero ng Cellphone/Telepono">
							</td>
						</tr>
						<tr>
							<td>
								<label>Kasarian</label>
								<table width="100%" cellspacing="0px" cellpadding="0px">
									<tr>
										<td><label><input type="radio" name="gender" value="Lalake" checked><span></span>Lalake</label></td>
										<td><label><input type="radio" name="gender" value="Babae"><span></span>Babae</label></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<label>Estado Sibil</label>
								<select name="civilstatus">
									<?php
									$options = array("Walang Asawa", "May Asawa", "Live-in", "Hiwalay", "Biyudo/Biyuda");
									foreach($options as $op) {
										echo '<option value="'.$op.'">'.$op.'</option>';
									}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<label>Petsa ng Kapanganakan</label>
								<input type="date" name="birthdate">
							</td>
						</tr>
						<tr>
							<td>
								<label>Blood Type</label>
								<select name="bloodtype">
									<?php
									$options = array("O-", "O+", "A-", "A+", "B-", "B+", "AB-", "AB+");
									foreach($options as $op) {
										echo '<option value="'.$op.'">'.$op.'</option>';
									}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<label>Posisyon ng Pamilya</label>
								<table width="100%" cellspacing="0px" cellpadding="0px">
									<tr><td colspan="2"><label><input type="radio" name="familyposition" value="Household Head" checked><span></span>Household Head</label></td></tr>
									<tr><td colspan="2"><label><input type="radio" name="familyposition" value="Asawa"><span></span>Asawa</label></td></tr>
									<tr><td colspan="2"><label><input type="radio" name="familyposition" value="Anak"><span></span>Anak</label></td></tr>
									<tr><td colspan="2"><label><input type="radio" name="familyposition" value="Lolo/Lola"><span></span>Lolo/Lola</label></td></tr>
									<tr><td><label><input type="radio" name="familyposition" value="Iba pa"><span></span>Iba pa:</td><td><input type="text" name="familyposition_str"></label></td></tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<div class="card">
					<h4>Pangkalusugan</h4>
					<table class="form-container">
						<tr>
							<td>
								<label>Klasipikasyon ayon sa edad</label>
								<select name="ageclassification">
									<?php
									$options = $lib->ageClassification;
									$first = true;
									foreach($options as $op) {
										echo '<option value="'.$op.'">'.$op.'</option>';
									}
									?>
								</select>
							</td>
						</tr>
					</table>
				</div>
				<div class="card">
					<table class="form-container">
						<tr>
							<td>
								<label>Uri ng Palikuran</label>
								<select name="toilet">
									<?php
									$options = array("De buhos (Kubetang may poso negro)", "De buhos (Kubetang walang poso negro)", "Pit privy (Butas)", "Wala");
									$first = true;
									foreach($options as $op) {
										echo '<option value="'.$op.'">'.$op.'</option>';
									}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<label>Pinanggagalingan ng inuming tubig</label>
								<select name="watersource">
									<?php
									$options = array("Level 1. POSO", "Level 2. Ito ang gripong pinagkukunan ng lima o higit pang pamilya.", "Level 3. Nawasa o water district", "WRS (Binibili sa water refilling station)");

									$first = true;
									foreach($options as $op) {
										echo '<option value="'.$op.'">'.$op.'</option>';
									}
									?>
								</select>
							</td>
						</tr>
					</table>
				</div>
				<div class="card button-container compact">
					<input type="submit" class="block" style="padding: 20px">
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
} elseif(isset($_GET['edit'])) {
	$id = $_GET['edit'];
	$data = $lib->getRow("Household", "*", "HouseholdID = '$id'");
	if(!empty($data)) {
?>
<div id="body-container">
	<div class="content">
		<div class="bg-cover"></div>
		<div class="title">
			<h1>Edit Household Record</h1>
		</div>
		<div class="wrapper">
			<div class="col-6 offset-2" id="frmApplication">
				<form action="process.php?action=edithousehold" method="post">
				<div class="card">
					<h4>Identification</h4>
					<table class="form-container">
						<tr>
							<td>
								<label>Barangay</label>
								<select name="barangayid">
								<?php
								$options = $lib->getData("Barangay", "*", "1=1 ORDER BY City, Name");
								foreach($options as $op) {
									$selected = "";
									if($op == $data["BarangayID"])
										$selected = " selected";
									echo '<option value="'.$op["ID"].'"'.$selected.'>Barangay '.$op["Name"].', '.$op["City"].'</option>';
								}
								?>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<label>PhilHealth No.</label>
								<input type="text" name="philhealth" placeholder="PhilHealth No." value="<?php echo $data["PhilHealth"]; ?>">
							</td>
						</tr>
						<tr>
							<td>
								<label>NHTS</label>
								<br>
								<table width="100%">
									<tr>
									<?php
									$options = array("Oo", "Hindi");
									foreach($options as $op) {
										$selected = "";
										if($op == $data["NHTS"])
											$selected = " checked";
									?>
										<td><label><input type="radio" name="nhts" value="<?php echo $op; ?>"<?php echo $selected; ?>><span></span><?php echo $op; ?></label></td>
									<?php
									}
									?>
										
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<label>Household ID</label>
								<input type="text" name="householdid" placeholder="Household ID" value="<?php echo $data["HouseholdID"]; ?>">
							</td>
						</tr>
					</table>
				</div>
				<div class="card">
					<h4>Personal Information</h4>
					<table class="form-container">
						<tr>
							<td>
								<label>Buong Pangalan</label>
								<table width="100%" cellspacing="0px" cellpadding="0px">
									<tr>
										<td><input type="text" name="firstname" placeholder="First Name" value="<?php echo $data["FirstName"]; ?>"></td>
										<td><input type="text" name="middlename" placeholder="Middle Name" value="<?php echo $data["MiddleName"]; ?>"></td>
										<td><input type="text" name="lastname" placeholder="Last Name" value="<?php echo $data["LastName"]; ?>"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<label>Tirahan</label>
								<table width="100%" cellspacing="0px" cellpadding="0px">
									<tr>
										<td><input type="text" name="nostreet" placeholder="No. Street" value="<?php echo $data["NoStreet"]; ?>"></td>
										<td><input type="text" name="barangay" placeholder="Barangay" value="<?php echo $data["Barangay"]; ?>"></td>
									</tr>
									<tr>
										<td>
											<select name="city">
												<?php
												$cities = $lib->cities;
												foreach($cities as $city) {
													$selected = "";
													if($city == $data["City"])
														$selected = " selected";
													echo '<option value="'.$city.'"'.$selected.'>'.$city.'</option>';
												}
												?>
											</select>
										</td>
										<td><input type="text" name="province" placeholder="Province" value="<?php echo $data["Province"]; ?>"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<label>Numero ng Cellphone/Telepono</label>
								<input type="text" name="mobileno" placeholder="Numero ng Cellphone/Telepono" value="<?php echo $data["MobileNo"]; ?>">
							</td>
						</tr>
						<tr>
							<td>
								<label>Kasarian</label>
								<table width="100%" cellspacing="0px" cellpadding="0px">
									<tr>
									<?php
									$options = array("Lalake", "Babae");
									foreach($options as $op) {
										$checked = "";
										if($op == $data["Gender"])
											$checked = " checked";
										echo '<td><label><input type="radio" name="gender" value="'.$op.'"'.$checked.'><span></span>'.$op.'</label></td>';
									}
									?>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<label>Estado Sibil</label>
								<select name="civilstatus">
									<?php
									$options = array("Walang Asawa", "May Asawa", "Live-in", "Hiwalay", "Biyudo/Biyuda");
									foreach($options as $op) {
										$selected = "";
										if($op == $data["CivilStatus"])
											$selected = " selected";
										echo '<option value="'.$op.'"'.$selected.'>'.$op.'</option>';
									}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<label>Petsa ng Kapanganakan</label>
								<input type="date" name="birthdate" value="<?php echo $data["BirthDate"]; ?>">
							</td>
						</tr>
						<tr>
							<td>
								<label>Blood Type</label>
								<select name="bloodtype">
									<?php
									$options = array("O-", "O+", "A-", "A+", "B-", "B+", "AB-", "AB+");
									foreach($options as $op) {
										$selected = "";
										if($op == $data["BloodType"])
											$selected = " selected";
										echo '<option value="'.$op.'"'.$selected.'>'.$op.'</option>';
									}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<label>Posisyon ng Pamilya</label>
								<table width="100%" cellspacing="0px" cellpadding="0px">
								<?php
								$options = array("Household Head", "Asawa", "Anak", "Lolo/Lola");
								foreach($options as $op) {
									$checked = "";
									if($op == $data["FamilyPosition"])
										$checked = " checked";
								?>
									<tr><td colspan="2"><label><input type="radio" name="familyposition" value="<?php echo $op; ?>"<?php echo $checked; ?>><span></span><?php echo $op; ?></label></td></tr>
								<?php
								}
								if(!in_array($data["FamilyPosition"], $options)) {
								?>
									<tr><td><label><input type="radio" name="familyposition" value="Iba pa" checked><span></span>Iba pa:</td><td><input type="text" name="familyposition_str" value="<?php echo $data["FamilyPosition"]; ?>"></label></td></tr>
								<?php
								} else {
								?>
									<tr><td><label><input type="radio" name="familyposition" value="Iba pa"><span></span>Iba pa:</td><td><input type="text" name="familyposition_str"></label></td></tr>
								<?php
								}
								?>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<div class="card">
					<h4>Pangkalusugan</h4>
					<table class="form-container">
						<tr>
							<td>
								<label>Klasipikasyon ayon sa edad</label>
								<select name="ageclassification">
									<?php
									$options = $lib->ageClassification;
									$first = true;
									foreach($options as $op) {
										$selected = "";
										if($op == $data["AgeClassification"])
											$selected = " selected";
										echo '<option value="'.$op.'"'.$selected.'>'.$op.'</option>';
									}
									?>
								</select>
							</td>
						</tr>
					</table>
				</div>
				<div class="card">
					<table class="form-container">
						<tr>
							<td>
								<label>Uri ng Palikuran</label>
								<select name="toilet">
									<?php
									$options = array("De buhos (Kubetang may poso negro)", "De buhos (Kubetang walang poso negro)", "Pit privy (Butas)", "Wala");
									$first = true;
									foreach($options as $op) {
										$selected = "";
										if($op == $data["Toilet"])
											$selected = " selected";
										echo '<option value="'.$op.'"'.$selected.'>'.$op.'</option>';
									}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<label>Pinanggagalingan ng inuming tubig</label>
								<select name="watersource">
									<?php
									$options = array("Level 1. POSO", "Level 2. Ito ang gripong pinagkukunan ng lima o higit pang pamilya.", "Level 3. Nawasa o water district", "WRS (Binibili sa water refilling station)");

									$first = true;
									foreach($options as $op) {
										$selected = "";
										if($op == $data["WaterSource"])
											$selected = " selected";
										echo '<option value="'.$op.'"'.$selected.'>'.$op.'</option>';
									}
									?>
								</select>
							</td>
						</tr>
					</table>
				</div>
				<div class="card button-container compact">
					<input type="submit" class="block" style="padding: 20px">
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
	}
} else {
?>
<div id="body-container">
	<div class="content">
		<div class="bg-cover"></div>
		<div class="title">
			<h1>Household Master Data</h1>
		</div>
		<div class="wrapper">
			<div class="col-3">
				<div class="card" id="frmSearch">
					<h4>Search</h4>
					<table class="form-container">
						<tr>
							<td><label>Name or ID</label>
							<input type="text" name="name" placeholder="Name or ID"></td>
						</tr>
						<tr>
							<td><label>Barangay</label>
							<select name="barangay">
								<option value="any">Any barangay</option>
								<?php
								$options = $lib->getData("Barangay", "*", "1=1");
								foreach($options as $op) {
									echo '<option value="'.$op["ID"].'">'.$op["Name"].', '.$op["City"].'</option>';
								}
								?>
							</select><input type="hidden" name="p" value="1"></td>
						</tr>
						<tr>
							<td><label>Results per page</label>
							<select name="pp">
							<?php
							$options = array(25,100,250,"All");
							foreach($options as $option) {
								echo '<option value="'.$option.'">'.$option.'</option>';
							}
							?>
							</select></td>
						</tr>
					</table>
					<ul class="button-container block">
						<li><a id="btnSearch" class="raised_button">Search</a></li>
					</ul>
				</div>
			</div>
			<div class="col-7" id="lstPatient">
			</div>
			<script>
			function refreshListPatient() {
				$checkedData = [];
				$("#numDataSelected").html("");
				showDataAction(false);

				$("#lstPatient").html('<div class="card"><center><br><br><img src="images/skin/oslo/bg/loading.gif" /><br><br></center></div>');
				$name = $("#frmSearch input[name=name]").val();
				$barangay = $("#frmSearch select[name=barangay]").val();
				$p = $("#frmSearch input[name=p]").val();
				$pp = $("#frmSearch select[name=pp]").val();
				$.ajax({
					type: "post",
					cache: true,
					url: "process.php?action=listhousehold",
					data: {p: $p, pp: $pp, name: $name, barangay: $barangay},
					success: function(html) {
						$("#lstPatient").html(html);
					}
				})
			}
			$(document).ready(function() {
				$("#frmSearch #btnSearch").click(function() {
					refreshListPatient();
				})
			})
			refreshListPatient();
			</script>
		</div>
	</div>
</div>
<?php
}
?>