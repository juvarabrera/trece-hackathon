<div id="body-container">
	<div class="content">
		<div class="bg-cover"></div>
		<div class="title">
			<h1>Alerts Overview</h1>
		</div>
		<div class="wrapper">
			<div class="col-7" id="lstAlerts">
			</div>
			<script>
			function refreshListAlerts() {
				$checkedData = [];
				$("#numDataSelected").html("");
				showDataAction(false);

				$("#lstAlerts").html('<div class="card"><center><br><br><img src="images/skin/oslo/bg/loading.gif" /><br><br></center></div>');
				$name = $("#frmSearch input[name=name]").val();
				$barangay = $("#frmSearch select[name=barangay]").val();
				$p = $("#frmSearch input[name=p]").val();
				$pp = $("#frmSearch select[name=pp]").val();
				$.ajax({
					type: "post",
					cache: true,
					url: "process.php?action=refreshListAlerts",
					data: {id: '<?php echo $loggedID; ?>'},
					success: function(html) {
						$("#lstAlerts").html(html);
					}
				})
			}
			refreshListAlerts();
			</script>
		</div>
	</div>
</div>