-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2015 at 02:05 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hackathon`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `ID` int(18) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `Type` varchar(20) NOT NULL,
  `Barangay` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=201500003 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`ID`, `Username`, `Password`, `Type`, `Barangay`) VALUES
(1, 'admin', 'admin', 'Administrator', NULL),
(201500001, 'bhw-test', '123456', 'BHW', '//8//9//5//'),
(201500002, 'rhm-test', '123', 'RHM', '//10//5//');

-- --------------------------------------------------------

--
-- Table structure for table `alert`
--

CREATE TABLE IF NOT EXISTS `alert` (
  `ID` int(12) NOT NULL,
  `FromID` int(12) NOT NULL,
  `ToID` int(12) NOT NULL,
  `HouseholdID` int(12) NOT NULL,
  `Message` varchar(1000) NOT NULL,
  `DateSent` date NOT NULL,
  `TimeSent` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `barangay`
--

CREATE TABLE IF NOT EXISTS `barangay` (
  `ID` int(12) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `City` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barangay`
--

INSERT INTO `barangay` (`ID`, `Name`, `City`) VALUES
(5, 'San Francisco', 'General Trias'),
(10, 'Abrera', 'Bacoor');

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis_bk42`
--

CREATE TABLE IF NOT EXISTS `diagnosis_bk42` (
  `ID` int(18) NOT NULL,
  `HomeBasedRecord` varchar(100) NOT NULL,
  `DateLabor` date NOT NULL,
  `Location` varchar(100) NOT NULL,
  `Person` varchar(100) NOT NULL,
  `E1` varchar(1000) DEFAULT NULL,
  `E2_1` date DEFAULT NULL,
  `E2_2` date DEFAULT NULL,
  `E2_3` date DEFAULT NULL,
  `E3` varchar(100) NOT NULL,
  `E4` varchar(100) NOT NULL,
  `E5` varchar(100) NOT NULL,
  `ServiceSource` varchar(100) NOT NULL,
  `FamilyPlanning` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis_bkb`
--

CREATE TABLE IF NOT EXISTS `diagnosis_bkb` (
  `ID` int(18) NOT NULL,
  `HomeBasedRecord` varchar(100) NOT NULL,
  `D1` varchar(1000) DEFAULT NULL,
  `NumPregnancy` int(2) NOT NULL,
  `LastPeriod` date NOT NULL,
  `ExpectedLabor` date NOT NULL,
  `D2` varchar(1000) NOT NULL,
  `Visit13` date DEFAULT NULL,
  `Visit46` date DEFAULT NULL,
  `Visit79` date DEFAULT NULL,
  `Tetanus1` date DEFAULT NULL,
  `Tetanus2` date DEFAULT NULL,
  `IronSupplements` date DEFAULT NULL,
  `D5` varchar(1000) DEFAULT NULL,
  `D6` varchar(10) NOT NULL,
  `D7` varchar(1000) DEFAULT NULL,
  `Result` varchar(100) NOT NULL,
  `ResultDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis_bp`
--

CREATE TABLE IF NOT EXISTS `diagnosis_bp` (
  `ID` int(18) NOT NULL,
  `HomeBasedRecord` varchar(100) NOT NULL,
  `Weight` decimal(12,2) NOT NULL,
  `Height` decimal(12,2) NOT NULL,
  `Location` varchar(100) DEFAULT NULL,
  `LocationStr` varchar(100) DEFAULT NULL,
  `A1` varchar(1000) DEFAULT NULL,
  `A2_1` date DEFAULT NULL,
  `A2_2` date DEFAULT NULL,
  `A2_3` date DEFAULT NULL,
  `A3` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis_s`
--

CREATE TABLE IF NOT EXISTS `diagnosis_s` (
  `ID` int(18) NOT NULL,
  `HomeBasedRecord` varchar(100) NOT NULL,
  `WeightBefore` decimal(12,2) NOT NULL,
  `Weight` decimal(12,2) NOT NULL,
  `HeightBefore` decimal(12,2) NOT NULL,
  `Height` decimal(12,2) NOT NULL,
  `B1` varchar(1000) DEFAULT NULL,
  `BCG` date DEFAULT NULL,
  `HepaB1` date DEFAULT NULL,
  `Penta1` date DEFAULT NULL,
  `Penta2` date DEFAULT NULL,
  `Penta3` date DEFAULT NULL,
  `OPV1` date DEFAULT NULL,
  `OPV2` date DEFAULT NULL,
  `OPV3` date DEFAULT NULL,
  `Rota1` date DEFAULT NULL,
  `Rota2` date DEFAULT NULL,
  `B3` varchar(1000) DEFAULT NULL,
  `B4` varchar(1000) DEFAULT NULL,
  `ServiceSource` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis_sc`
--

CREATE TABLE IF NOT EXISTS `diagnosis_sc` (
  `ID` int(18) NOT NULL,
  `HomeBasedRecord` varchar(100) DEFAULT NULL,
  `FluVaccine` date DEFAULT NULL,
  `PneuVaccine` date DEFAULT NULL,
  `ServiceSoure` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis_u14b`
--

CREATE TABLE IF NOT EXISTS `diagnosis_u14b` (
  `ID` int(18) NOT NULL,
  `HomeBasedRecord` varchar(100) NOT NULL,
  `Symptoms` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis_u15u`
--

CREATE TABLE IF NOT EXISTS `diagnosis_u15u` (
  `ID` int(18) NOT NULL,
  `HomeBasedRecord` varchar(100) NOT NULL,
  `Cough` varchar(10) NOT NULL,
  `G1` varchar(100) DEFAULT NULL,
  `G2` varchar(100) DEFAULT NULL,
  `G3` varchar(100) DEFAULT NULL,
  `Complete` varchar(10) DEFAULT NULL,
  `Reason` varchar(100) DEFAULT NULL,
  `ServiceSource` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis_uf`
--

CREATE TABLE IF NOT EXISTS `diagnosis_uf` (
  `ID` int(18) NOT NULL,
  `HomeBasedRecord` varchar(100) NOT NULL,
  `C1` varchar(1000) DEFAULT NULL,
  `BCG` date DEFAULT NULL,
  `HepaB1` date DEFAULT NULL,
  `Penta1` date DEFAULT NULL,
  `Penta2` date DEFAULT NULL,
  `Penta3` date DEFAULT NULL,
  `OPV1` date DEFAULT NULL,
  `OPV2` date DEFAULT NULL,
  `OPV3` date DEFAULT NULL,
  `Rota1` date DEFAULT NULL,
  `Rota2` date DEFAULT NULL,
  `Measles` date DEFAULT NULL,
  `C3` varchar(1000) DEFAULT NULL,
  `ServiceSource` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis_wra`
--

CREATE TABLE IF NOT EXISTS `diagnosis_wra` (
  `ID` int(18) NOT NULL,
  `HomeBasedRecord` varchar(100) DEFAULT NULL,
  `FPM` varchar(100) DEFAULT NULL,
  `SourceFPM` varchar(100) DEFAULT NULL,
  `F1` varchar(100) DEFAULT NULL,
  `F2` varchar(100) DEFAULT NULL,
  `F3` varchar(100) DEFAULT NULL,
  `F4` varchar(100) DEFAULT NULL,
  `F5` varchar(100) DEFAULT NULL,
  `F5_2` varchar(1000) DEFAULT NULL,
  `F6` varchar(100) DEFAULT NULL,
  `F7` varchar(100) DEFAULT NULL,
  `F8` varchar(1000) DEFAULT NULL,
  `FPMethod` varchar(10) DEFAULT NULL,
  `ServiceSource` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `household`
--

CREATE TABLE IF NOT EXISTS `household` (
  `HouseholdID` int(18) NOT NULL,
  `BarangayID` int(12) NOT NULL,
  `PhilHealth` int(12) DEFAULT NULL,
  `NHTS` varchar(10) NOT NULL,
  `FirstName` varchar(30) NOT NULL,
  `MiddleName` varchar(30) NOT NULL,
  `LastName` varchar(30) NOT NULL,
  `NoStreet` varchar(100) NOT NULL,
  `Barangay` varchar(100) NOT NULL,
  `City` varchar(100) NOT NULL,
  `Province` varchar(100) NOT NULL,
  `MobileNo` int(11) NOT NULL,
  `Gender` varchar(100) NOT NULL,
  `CivilStatus` varchar(100) NOT NULL,
  `BirthDate` varchar(100) NOT NULL,
  `BloodType` varchar(100) NOT NULL,
  `FamilyPosition` varchar(100) NOT NULL,
  `AgeClassification` varchar(100) NOT NULL,
  `Toilet` varchar(100) NOT NULL,
  `WaterSource` varchar(100) NOT NULL,
  `HealthStatus` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `alert`
--
ALTER TABLE `alert`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `barangay`
--
ALTER TABLE `barangay`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `diagnosis_bk42`
--
ALTER TABLE `diagnosis_bk42`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `diagnosis_bkb`
--
ALTER TABLE `diagnosis_bkb`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `diagnosis_bp`
--
ALTER TABLE `diagnosis_bp`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `diagnosis_s`
--
ALTER TABLE `diagnosis_s`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `diagnosis_sc`
--
ALTER TABLE `diagnosis_sc`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `diagnosis_u14b`
--
ALTER TABLE `diagnosis_u14b`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `diagnosis_u15u`
--
ALTER TABLE `diagnosis_u15u`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `diagnosis_uf`
--
ALTER TABLE `diagnosis_uf`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `diagnosis_wra`
--
ALTER TABLE `diagnosis_wra`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `household`
--
ALTER TABLE `household`
  ADD PRIMARY KEY (`HouseholdID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `ID` int(18) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=201500003;
--
-- AUTO_INCREMENT for table `alert`
--
ALTER TABLE `alert`
  MODIFY `ID` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `barangay`
--
ALTER TABLE `barangay`
  MODIFY `ID` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
